const { json } = require('express');
const express = require('express');
const app = express()
const connectDB = require('./connect/db')
const port = 3000;

const userRoute = require('./routes/user')

//middleware
app.use(express.json())

// routes 
app.use('/api/v1/user', userRoute)

const start = async () => {
    try {
        await connectDB()
        app.listen(port, console.log(`server is running on port : ${port}`))
    }catch(error){
        console.log(`error while starting server : ${error}`)
    }
}

start()