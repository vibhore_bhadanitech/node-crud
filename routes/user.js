const express = require('express')
const router = require('express').Router()
const { getUser, createUser } = require('../controllers/user')

router.route('/').get(getUser).post(createUser);
// router.route('/create').post(createUser)


module.exports = router
