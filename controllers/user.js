const getUser = (req, res, next) => {
   let user = {
       name: 'ABC',
       age: 20,
       phone: 6436712434761
   }
    // console.log(req.params)
    // next()
    res.status(200).json(user)
}

const createUser = (req, res, next) => {
    try{
        let user = req.body
        res.status(201).json(user)
    }catch (err){
        console.log(err)
    }
}

module.exports = {
    getUser,
    createUser
}