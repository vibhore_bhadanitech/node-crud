const mysql = require('mysql')

const connectDB = () => {
    return mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'password',
        database:'employeedb',
        multipleStatements: true
    })
}

module.exports = connectDB